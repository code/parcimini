Parcimini
=========

Refresh GnuPG keyring slowly, in random order, with random waits between each
refresh, looping indefinitely.  Intended as an incompatible alternative for
[parcimonie](https://github.com/EtiennePerot/parcimonie.sh).

You will almost certainly want `use-tor` in your `dirmngr.conf` for this to be
useful at all.

License
-------

Copyright (C) 2019--2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.
