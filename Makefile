.PHONY: all install clean
.SUFFIXES:
.SUFFIXES: .bash
ALL = parcimini
BASH = /bin/bash
PREFIX = /usr/local
all: $(ALL)
.bash:
	$(BASH) -c :
	awk -v interpreter=$(BASH) 'NR == 1 { $$1 ="#!" interpreter } 1' $< > $@
	chmod +x ./$@
install: parcimini
	mkdir -p -- $(PREFIX)/bin
	cp -- parcimini $(PREFIX)/bin
clean:
	rm -f -- $(ALL)
