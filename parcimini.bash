#!bash
#
# Copyright (C) 2019--2021 Tom Ryder <tom@sanctum.geek.nz>
#
# This file is part of doomsh.
#
# doomsh is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# doomsh is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# doomsh.  If not, see <https://www.gnu.org/licenses/>.

# parcimini: Refresh GnuPG keyring via Tor avoiding correlation attacks
self=parcimini

# If systemd made us a logs directory, write to files in there
if [ -n "$LOGS_DIRECTORY" ] ; then
    dir=${LOGS_DIRECTORY%%:%}
    exec >>"$dir"/"$self".log
fi

# Base interval between key retrievals from first arg; default 20 mins
interval=${1:-120}

# Check we have gpg and shuf, neither of which are POSIX
hash gpg || exit
hash shuf || exit

# Define logging function
logf() {
    format=$1
    shift
    printf "%s: %s: $format" \
        "$(date +'%FT%T')" "$self" "$@"
}

# Make a temporary file for the key listings, delete on exit
cleanup() {
    logf 'Stopped\n'
    rm -f -- "$list"
}
trap cleanup EXIT
list=$(mktemp) || exit

# Define a function to retrieve all keychain fingerprints
key_ids() {
    gpg --batch --no-tty --list-keys --with-colons |
    awk 'BEGIN { FS = ":" }
$1 == "pub" { pub = 1 ; next }
$1 == "fpr" && pub { pub = 0 ; key_ids[$(NF-1)]++ }
END { for (key_id in key_ids) print key_id }'
}

# Log process start
logf 'Started; base interval %u seconds.\n' \
    "$interval"

# While we're able to write a shuffled key list to the file, refresh all of them
while key_ids | shuf > "$list" ; do
    logf 'Beginning new round; %u key IDs found.\n' \
        "$(sed '$=;d' "$list")"

    # Shuffle list and read each ID
    while read -r key_id ; do

        # Sleep for a random interval
        spell=$((RANDOM % interval + 1))
        logf 'Sleeping for %u seconds...\n' \
            "$spell"
        sleep "$spell"

        # Retrieve key
        logf 'Retrieving key %s...\n' \
            "$key_id"
        gpg --batch --no-tty --recv-key "$key_id" 2>&1

    done < $list
done
